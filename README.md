# React Native - GDC technical test

We're looking for front-end developers who can build awesome web apps, so we have created this test in order to evaluate your skills.

## Instructions

#### 1. Build a cool app

You have 3 days to complete and send the test, but it should not take more than 3-4 hours of your time in total.

*Implementation and design will be evaluated.*

#### 2. Deliverable

* Setup your Development Environment ([React Native - Getting Started guide](https://facebook.github.io/react-native/docs/getting-started.html))
* Clone this repository (make sure it's private) through Bitbucket
* Share your repository with `devs-gdc`
* Create and checkout a new branch
* Commit your changes on it
* Issue a Pull Request on your private repository

Do not hesitate to complete your pull request description with any extra info you might want to share with us.

## Context

GDC exposes a public API that checks if an email belongs to a member.

## Goal

Create a React Native app where we can classify people emails as « cool » or « bad ».

## Requirements

Your app should:

* Check if an email belongs to GDC
* Classify people emails in 2 different lists:
  * « Cool Guys » who are GDC members
  * « Bad Guys » who are not GDC members
* Do not duplicates items (people email) in lists

As far as we know, `nicolas@gensdeconfiance.fr` & `ulric@gensdeconfiance.fr` are cool guys.

Ugly mockup that might help or disturb you:
![Ugly Mockup](/technical_test_mockup.png)


## API info

* API signature `
https://gensdeconfiance.fr/api/member/check.json?apiPartnerId=[partnerId]&apiPartnerSecret=[partnerSecret]&query=[email]
`

We can provide API partner credentials (`apiPartnerId` / `apiPartnerSecret`) by email :)

#### Bonus:

* Persist data using Redux
* Test your code using Jest
* Include animations
* Make it beautiful
* Include doc
* ...

Happy coding !